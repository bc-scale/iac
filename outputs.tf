output "gitlab_sub_groups" {
    value = gitlab_group.sub_groups

    sensitive = true
}

output "gitlab_projects" {
    #value = gitlab_project.projects.*.ssh_url_to_repo
    value       = gitlab_project.projects
    sensitive = true
}