

variable "gitlab_projects" {
    type = map(object({
        name                                = string
        description                         = string
        issues_enabled                      = bool
        packages_enabled                    = bool
        pipelines_enabled                   = bool
        topics                              = list(string)
        visibility_level                    = string
        wiki_enabled                        = bool
        parent_group                        = string
    }))
}

variable "gitlab_root_groups" {
    type = map(number)  
}

variable "gitlab_sub_groups" {
    type = map(object({
        name                                = string
        description                         = string
        path                                = string
        parent_group                        = string
        visibility_level                    = string
        prevent_forking_outside_group       = bool
    }))
}

variable "gitlab_group_variables" {
    type = map(object({
        secret                              = string
    }))
}