gitlab_projects = {

  "iac" = {
    issues_enabled      = true
    name                = "gitlab-core"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "terraform", "gitlab" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "gitlab"
  },
  
  "linode_compute" = {
    issues_enabled      = true
    name                = "linode_compute"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "terraform", "linode", "compute" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "linode"
  },
  
  "linode_dns" = {
    issues_enabled      = true
    name                = "linode_dns"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "terraform", "linode", "dns" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "linode"
  },
  
  "workstations_home" = {
    issues_enabled      = true
    name                = "home"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "workstations", "home" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "workstations"
  },

  "workstations_scripts" = {
    issues_enabled      = true
    name                = "scripts"
    description         = "personal scripts"
    packages_enabled    = false
    pipelines_enabled   = true
    topics              = [ "iac", "bash", "workstations", "scripts" ]
    visibility_level    = "private"
    wiki_enabled        = false
    parent_group        = "workstations"
  },
  
  "usenet_mono" = {
    issues_enabled      = true
    name                = "usenet_mono"
    description         = "Monolith containing the complete usenet stack"
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "usenet", "monolith", "plex", "sabnzbd", "lidarr", "radarr", "sonarr" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "usenet"
  },
  
  "workstations_core" = {
    issues_enabled      = true
    name                = "core"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "workstations", "core" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "workstations"
  },
  
  "workstations_desktop" = {
    issues_enabled      = true
    name                = "desktop"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "workstations", "desktop" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "workstations"
  },
  
  "workstations_restic" = {
    issues_enabled      = true
    name                = "restic"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "workstations", "restic" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "workstations"
  },
  
  "grocy" = {
    issues_enabled      = true
    name                = "grocy"
    description         = ""
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "selfhosted", "grocy" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "selfhosted"
  },
  
  "paperless-ngx" = {
    issues_enabled      = true
    name                = "paperless-ngx"
    description         = "Paperless NGX Deployment"
    packages_enabled    = true
    pipelines_enabled   = true
    topics              = [ "iac", "ansible", "selfhosted", "paperless" ]
    visibility_level    = "public"
    wiki_enabled        = false
    parent_group        = "selfhosted"
  }

}

gitlab_root_groups = {
  "iac"                 = 55136666
}

gitlab_sub_groups = {
  
  "linode" = {
    description                     = "repositories that manage my linodes"
    name                            = "linode"
    parent_group                    = "iac"
    path                            = "linode"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  },
  
  "gitlab" = {
    description                     = "repositories that manage my gitlabery"
    name                            = "gitlab"
    parent_group                    = "iac"
    path                            = "gitlab"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  },
  
  "github" = {
    description                     = "repositories that manage my github presence"
    name                            = "github"
    parent_group                    = "iac"
    path                            = "github"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  },
  
  "workstations" = {
    description                     = "repositories that manage my personal computing exerpience"
    name                            = "workstations"
    parent_group                    = "iac"
    path                            = "workstations"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  },
  
  "usenet" = {
    description                     = "usenet deployment"
    name                            = "usenet"
    parent_group                    = "iac"
    path                            = "usenet"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  },
  
  "selfhosted" = {
    description                     = "A variety of self-hosted services"
    name                            = "selfhosted"
    parent_group                    = "iac"
    path                            = "selfhosted"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  },
  
  "ansible" = {
    description                     = "Ansible IaC Development - Collections"
    name                            = "ansible"
    parent_group                    = "iac"
    path                            = "ansible"
    prevent_forking_outside_group   = true
    visibility_level                = "public"
  }

}

gitlab_group_variables = {
}


