# Add management for this Project
resource "gitlab_project" "projects" {
  for_each                      = var.gitlab_projects

  name                          = each.value.name
  description                   = each.value.description
  packages_enabled              = each.value.packages_enabled
  pipelines_enabled             = each.value.pipelines_enabled
  topics                        = each.value.topics
  visibility_level              = each.value.visibility_level
  wiki_enabled                  = each.value.wiki_enabled
  namespace_id                  = gitlab_group.sub_groups[each.value.parent_group].id
}