# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs

data "gitlab_group" "groups" {
  for_each                      = var.gitlab_root_groups
  group_id                      = each.value
}

resource "gitlab_group" "sub_groups" {
  for_each                      = var.gitlab_sub_groups

  name                          = each.value.name
  path                          = each.value.path
  description                   = each.value.description
  parent_id                     = data.gitlab_group.groups[each.value.parent_group].id
  prevent_forking_outside_group = true
  visibility_level              = "public"
}

## Create any secrets needed, like LINODE TOKENS
resource "gitlab_group_variable" "iac" {
  for_each                      = var.gitlab_group_variables

  group                         = var.gitlab_root_groups["iac"]
  key                           = each.key
  value                         = each.value.secret
}