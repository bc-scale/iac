terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.15.1"
    }
  }
}

# Set GITLAB_TOKEN
provider "gitlab" {
  # Configuration options
}

