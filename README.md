
## Tokens

`GITLAB_TOKEN` - Needed to interact with GitLab Infrastructure
`LINODE_TOKEN` - Needed to interact with Linode Infrastructure

To set a group variable when running
```
$ terraform apply -auto-approve -var="gitlab_group_variables={\"GITLAB_TOKEN\"={secret=\"$GITLAB_TOKEN\"}}"
```